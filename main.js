document.querySelector('#shootButton').onclick = function () {
    let userChoice = document.getElementById('userInput').value

    let compChoice = Math.floor((Math.random() * 3))

    if (compChoice == 1) {
        compChoice = 'rock'
    } else if (compChoice == 2) {
        compChoice = 'paper'
    } else {
        compChoice = 'scissors'
    }

    function compare(choice1, choice2) {
        if (userChoice !== 'rock' && userChoice !== 'paper' && userChoice !== 'scissors') {
            alert ('Nice Try!, thats not an option! Try Again!')
        }

        if (choice1 === choice2) {
            alert ('You have tied the machine. Try again!')
        }

        if (choice1 === 'rock') {
            if (choice2 === 'scissors') {
                alert ('Congratulations Rock beats Scissors, you have defeated the machine!')
            } else {
                alert ('The machine chose paper, better luck next time!')
            }
        }

        if (choice1 === 'paper') {
            if (choice2 === 'rock') {
                alert ('Congratulations paper beats rock, you have won against the machine!')
            } else {
                alert ('The machines scissors cut you in half, you lose...')
            }
        }

        if (choice1 === 'scissors') {
            if (choice2 === 'rock') {
                alert ('The rock raises its eyebrow and shouts "can you smelllLLL my victory!" you lose...')
            } else {
                alert ('Congrats, who would of thought that paper beat rock, you win!')
            }
        }
    }

    compare(userChoice, compChoice)
}
